///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// A custom implementation of a single linked list
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06_APR_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include "node.hpp"
#include <cassert>
#include <iostream>

class DoubleLinkedList {
   protected:
      Node* head = nullptr;
      Node* tail = nullptr;
   private:
      unsigned int count = 0;
   public:
      const bool empty()            const;
      void push_front(Node*);
      void push_back(Node*);
      Node* pop_front();
      Node* pop_back();
      Node* get_first()             const;
      Node* get_last()              const;
      Node* get_next(const Node*)   const;
      Node* get_prev(const Node*)   const;
      inline unsigned int size()           const { return count; };
      void insert_after(Node*, Node*);
      void insert_before(Node*, Node*);
      void swap(Node*, Node*);
      const bool isSorted()         const;
      void insertionSort();
      bool validate()               const;
};
