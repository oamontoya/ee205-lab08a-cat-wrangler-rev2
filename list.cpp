///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// A custom implementation of a single linked list
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06_APR_2021
///////////////////////////////////////////////////////////////////////////////
#include "list.hpp"

//empty():determines whether the list is empty or not
const bool DoubleLinkedList::empty() const {
   //strictly checks for misassigned head and tail values
   //also uses the size() function to verify the size of the list is in fact zero
   return head == nullptr && tail == nullptr && size() == 0;
}

//push_front(): add a node to the bery front of the list
void DoubleLinkedList::push_front(Node* newNode) {
   //check that the node pointer passed in is not a null pointer
   if(newNode == nullptr) { return; } 
   //the general case(the list is not initially empty)
   if(!empty()) {
      newNode->next = head;
      newNode->prev = nullptr;
      head->prev = newNode;
      head = newNode;
      count++;
   }
   //the special case where the list is empty and this is the first node added
   else {
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
      count++;
   }
   validate();
}

//push_back(): add a node to the very end of the list
void DoubleLinkedList::push_back(Node* newNode) {
   //check that the node pointer passed in is not a null pointer
   if(newNode == nullptr) { return; }
   //the general case(the list is not initially empty)
   if(!empty()) {
      newNode->next = nullptr;
      newNode->prev = tail;
      tail->next = newNode;
      tail = newNode;
      count++;
   }
   //the special case where the list is empty and this is the first node added
   else {
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
      count++;
   }
   validate();
}

//pop_front(): removes the first node in the list 
Node* DoubleLinkedList::pop_front() {
   Node* removedNode = head;
   //special case: the list is empty
   if(size() == 0) { return removedNode; }
   //special case: the list has one node
   else if(size() == 1) {
      head = removedNode->next;
      tail = head;
      count--;
   }
   //general case: the list has more than one node
   else {
      head = removedNode->next;
      head->prev = nullptr;
      count--;
   }
   validate();
   return removedNode;
}

//pop_back(): removes the last node in the list
Node* DoubleLinkedList::pop_back() {
   Node* removedNode = tail;
   //special case: the list is empty
   if(size() == 0) { return removedNode; }
   //special case: the list has one node
   else if(size() == 1) {
      head = nullptr;
      tail = nullptr;
      count--;
   }
   //special case: the list has two nodes
   else if (size() == 2) {
      head->next = nullptr;
      tail = head;
      count--;
   }
   //general case: the list has more than two nodes
   else {
      tail = removedNode->prev;
      tail->next = nullptr;
      count--;
   }
   validate();
   return removedNode;
}

//get_first(): gets the head of the list
Node* DoubleLinkedList::get_first() const {
   return head;
}

//get_last(): gets the tail of the list
Node* DoubleLinkedList::get_last() const {
   return tail;
}

//get_next(): gets the next node from the node pointer passed in
Node* DoubleLinkedList::get_next(const Node* currentNode) const {
   return currentNode->next;
}

//get_prev(): gets the previous node from the node pointer passed in
Node* DoubleLinkedList::get_prev(const Node* currentNode) const {
   return currentNode->prev;
}

//insert_after(): add a node after another node in the list
void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode) {
   //special case: the node to be added is a null pointer
   if(newNode == nullptr) { return; }
   //special case: the node to be added is the same as the node to place behind
   if(currentNode == newNode) { return; }
   //special case: place the node after nullptr
   //nullptr in this context refers to the position before head
   if(currentNode == nullptr) { push_front(newNode); }
   //special case: place the node after the tail
   //if the list is empty, then currentNode = tail = nullptr, then the previous special case occurs and returns before this can execute
   else if(currentNode == tail) { push_back(newNode); }
   //general case: place the node behind some node between the head and the tail, head inclusive
   else {
      newNode->prev = currentNode;
      newNode->next = currentNode->next;
      currentNode->next->prev = newNode;
      currentNode->next = newNode;
      count++;
   }
   validate();
}

//insert_before(): add a node before another node in the list
void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode) {
   //special case: the node to be added is a null pointer
   if(newNode == nullptr) { return; }
   //special case: the node to be added is the same as the node to place behind
   if(currentNode == newNode) { return; }
   //special case: place the node before nullptr
   //nullptr in this context refers to the position after tail
   if(currentNode == nullptr) { push_back(newNode); }
   //special case: place the node before the head
   //if the list is empty, then currentNode = head = nullptr, then the previous special case occurs and returns before this can execute
   else if(currentNode == head) { push_front(newNode); }
   //general case: place the node in front of some node between the head and the tail, tail inclusive
   else {
      newNode->prev = currentNode->prev;
      newNode->next = currentNode;
      currentNode->prev->next = newNode;
      currentNode->prev = newNode;
      count++;
   }
   validate();
}

//swap(): swap two nodes in the list
void DoubleLinkedList::swap(Node* node1, Node* node2) {
   //special case: the nodes to be swapped are the same
   if(node1 == node2) { return; }
   //special case: the list is empty
   if(size() == 0) { return; }
   //special case: the list has only one node, so it is already sorted
   else if(size() == 1) { return; }
   //special case: two unique nodes from a list of two means the only nodes are head and tail
   else if(size() == 2) {
      //swap the list variables
      if(node1 == head && node2 == tail) { 
         head = node2;
         tail = node1;
      }
      else if(node1 == tail && node2 == head) {
         head = node1;
         tail = node2;
      }
      //fix up the pointers
      head->next = tail;
      tail->prev = head;
      head->prev = nullptr;
      tail->next = nullptr;
   
   }
   //general case: the list has more than 2 nodes, or the nodes are equal to each other
   else {
      //special case: node 1 and 2 are head and tail
      if((node1 == head && node2 == tail) || (node1 == tail && node2 == head)) {
         //swap the next and prev pointers
         head->next->prev = tail;
         tail->prev->next = head;
         //swap the list variables
         if(node1 == head && node2 == tail) { 
            head->prev = node2->prev;
            tail->next = node1->next;
            head->next = nullptr;
            tail->prev = nullptr;
            head = node2;
            tail = node1;
         }
         else if(node1 == tail && node2 == head) {
            head->prev = node1->prev;
            tail->next = node2->next;
            head->next = nullptr;
            tail->prev = nullptr;
            head = node1;
            tail = node2;
         }
      }
      //special case: only 1 of the nodes is either head or tail
      else if(((node1 == head) && (node2 != tail)) || ((node2 == head) && (node1 != tail)) || ((node1 == tail) && (node2 != head)) || ((node2 == tail) && (node1 != head))) {
         //special case: node 1 and 2 are consecutive, node 1 before node 2
         if(node1->next == node2) {
            if((node1 == head) && (node2 != tail)) {
               node1->next = node2->next;
               node1->next->prev = node1;
               node1->prev = node2;
               node2->next = node1;
               node2->prev = nullptr;
               head = node2;
            }
            else if((node2 == tail) && (node1 != head)) {
               node1->prev->next = node2;
               node2->prev = node1->prev;
               node2->next = node1;
               node1->prev = node2;
               node1->next = nullptr;
               tail = node1;
            }
         }
         //special case: node 1 and 2 are consecutive, node 2 before node 1
         else if(node2->next == node1) {
            if((node2 == head) && (node1 != tail)) {
               node2->next = node1->next;
               node2->next->prev = node1;
               node2->prev = node1;
               node1->next = node2;
               node1->prev = nullptr;
               head = node1;
            }
            else if((node1 == tail) && (node2 != head)) {
               node2->prev->next = node1;
               node1->prev = node2->prev;
               node1->next = node2;
               node2->prev = node1;
               node2->next = nullptr;
               tail = node2;
            }
         }
         //general case: the nodes are nonconsecutive
         else {  
            if((node1 == head) && (node2 != tail)) {
               Node* A = node2->next;

               node2->next->prev = node1;
               node2->prev->next = node1;
               node1->next->prev = node2;

               node2->next = node1->next;
               node1->next = A;
               node1->prev = node2->prev;
               node2->prev = nullptr;

               head = node2;
            }
            else if((node2 == head) && (node1 != tail)) {
               Node* A = node1->next;

               node1->next->prev = node2;
               node1->prev->next = node2;
               node2->next->prev = node1;

               node1->next = node2->next;
               node2->next = A;
               node2->prev = node1->prev;
               node1->prev = nullptr;

               head = node1;
            }
            else if((node1 == tail) && (node2 != head)) {
               Node* A = node2->prev;

               node2->prev->next = node1;
               node2->next->prev = node1;
               node1->prev->next = node2;

               node2->prev = node1->prev;
               node1->prev = A;
               node1->next = node2->next;
               node2->next = nullptr;

               tail = node2;
            }
            else if((node2 == tail) && (node1 != head)) {
               Node* A = node1->prev;

               node1->prev->next = node2;
               node1->next->prev = node2;
               node2->prev->next = node1;

               node1->prev = node2->prev;
               node2->prev = A;
               node2->next = node1->next;
               node1->next = nullptr;

               tail = node1;
            }
         }
      }
      //general case: neither of the nodes are head nor tail
      else { 
         //special case: node 1 and 2 are consecutive, node 1 before node 2
         if(node1->next == node2) {
            node1->prev->next = node2;
            node2->next->prev = node1;
            node1->next = node2->next;
            node2->prev = node1->prev;
            node1->prev = node2;
            node2->next = node1;
         }
         //special case: node 1 and 2 are consecutive, node 2 before node 1
         else if(node2->next == node1) {
            node2->prev->next = node1;
            node1->next->prev = node2;
            node2->next = node1->next;
            node1->prev = node2->prev;
            node2->prev = node1;
            node1->next = node2;
         }
         //general case: the nodes are nonconsecutive
         else {
            Node* A = node1->prev;
            Node* B = node1->next;
            Node* C = node2->prev;
            Node* D = node2->next;

            node1->next = D;
            node1->prev = C;
            node2->next = B;
            node2->prev = A;

            A->next = node2;
            B->prev = node2;
            C->next = node1;
            D->prev = node1;
         }
      }  
   }
}

//isSorted(): determines whether the list is sorted
const bool DoubleLinkedList::isSorted() const {
   if(size() <= 1)
      return true;
   for(Node* i = head ; i->next != nullptr; i = i->next) {
      if(*i > *i->next)
         return false;
   }
   return true;
}

//insertionSort(): implement an insertion sort on the list
void DoubleLinkedList::insertionSort() {
   Node* currentNode = head;
   Node* nextNode = nullptr;
   Node* smallestNode = nullptr;

   //repeat until every node in the list has been processd
   while(currentNode != nullptr) {
      smallestNode = currentNode;
      nextNode = currentNode->next;
      //compare the current smallest node with the next node
      while(nextNode != nullptr) {
         if(*smallestNode > *nextNode)
            smallestNode = nextNode;
         nextNode = nextNode->next;
      }
      //swap the nodes, which could be the same node, the next node, or nullptr
      swap(currentNode, smallestNode);
      //move to the next node in the sequence, or off the end of the list if all nodes are sorted
      currentNode = smallestNode->next;
   }
}

//validate(): sanity check the list
bool DoubleLinkedList::validate() const {
   //check for the list being empty
   //an empty list has: head = tail = nullptr, size of zero, and empty() returns true
   if(head == nullptr) {
      assert(tail == nullptr);
      assert(size() == 0);
      assert(empty());
   }
   //otherwise the list must not be empty
   else {
      assert(tail != nullptr);
      //the only time a non empty list head = tail is a list of size 1
      if(head == tail) { assert(size() == 1); }
      else { assert(size() > 1); }
      assert(!empty());
   }
   //iterate forwards through the list, counting each node in the list
   unsigned int forwardCount = 0;
   Node* currentNode = head;
   while(currentNode != nullptr) {
      forwardCount++;
      //ensure the link between the current node and the node in front of it is solid in both directions
      if(currentNode->next != nullptr) { assert(currentNode->next->prev == currentNode); }
      currentNode = currentNode->next;
   }
   //verify the size() function and the iterated count agree
   assert(this->size() == forwardCount);

   //iterate backwards through the list, counting each node in the list
   unsigned int backwardCount = 0;
   currentNode = tail;
   while(currentNode != nullptr) {
      backwardCount++;
      //ensure the link between the current node and the node behind it is solid in both directions
      if(currentNode->prev != nullptr) { assert(currentNode->prev->next == currentNode); }
      currentNode = currentNode->prev;
   }
   //verify the size() function and the iterated count agree
   assert(size() == backwardCount);

   return true;
}
